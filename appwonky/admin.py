from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(Departamento)
admin.site.register(Carrera)
admin.site.register(Materia)
admin.site.register(Profesor)
admin.site.register(HorarioComision)
admin.site.register(Comision)
admin.site.register(Alumnxs)
admin.site.register(InscripcionMateria)