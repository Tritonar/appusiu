from django.db import models
from django.db.models import Model
from django.utils.translation import gettext as _
import uuid , datetime , time
# Create your model

class Departamento(models.Model):
    codigoDepartamento = models.PositiveIntegerField(verbose_name="Codigo de Departamento",
        primary_key = True)
    name = models.CharField(verbose_name="Departamento",max_length=120)
    decano = models.CharField(verbose_name="Decano del Departamento:",max_length=120)
    def __str__(self):
        return self.name

class Carrera(models.Model):
    DCARRERA = [(1,_("1 cuatrimestre")),(2,_("1 año")),(3,_("1 año y medio")),
    (4,_("2 años")),(5,_("2 años y medio")),(6,_("3 años")),(7,_("3 años y medio")),
    (8,_("4 años")),(9, _("4 años y medio")),(10, _("5 años")),(11, _("5 años y medio"))]
    codigoCarrera = models.PositiveIntegerField(verbose_name="Codigo de Carrera",primary_key = True)
    name = models.CharField(verbose_name="Carrera",max_length=120)
    cuatrimestres= models.IntegerField(choices=DCARRERA,verbose_name="Duración")
    departamentoCarrera = models.ManyToManyField(Departamento)
    def __str__(self):
        return self.name

class Materia(models.Model):
    #codigoMateria,name,cargaHoraria,activa,carrerx
    codigoMateria = models.PositiveIntegerField(verbose_name="Codigo de Materia",
        primary_key = True)
    name = models.CharField(verbose_name="Materia",max_length=120,
        help_text="Ingrese el nombre de la Materia")
    cargaHoraria = models.TimeField(default='04:00')
    activa = models.BooleanField(default=True)
    carrera = models.ManyToManyField(Carrera)
    def Get_Carrera_Name(self):
        return '\n'.join([p.name for p in self.carrera.all()])
    def label(self):
        return self.name+'  '+str(self.Get_Carrera_Name())
    def __str__(self):
        return self.name+'  [ '+str(self.Get_Carrera_Name())+' ]'

class Profesor(models.Model):
    #'numeroLegajo','name','apellido','eemail','activa'
    numeroLegajo = models.PositiveIntegerField(primary_key=True,verbose_name="Numero de Legajo")
    name = models.CharField(verbose_name="Nombre",max_length=120)
    apellido = models.CharField(verbose_name="Apellido",max_length=60)
    eemail = models.EmailField(verbose_name="e-mail")
    celular = models.CharField(verbose_name="Celular",
        help_text="Sin el 15. ejemplo: 11-2020-2020",max_length=60)
    activa = models.BooleanField(verbose_name="Profesor Activo",default=True)
    def label(self):
        return "%s %s" % (self.name,self.apellido)
    def __str__(self):
        return"%s %s" % (self.name,self.apellido)

class HorarioComision(models.Model):
    #('id','dia','horaDeInicio','horaDeFin')
    DIASEMANA = ( (None, _('No Definido')),('Lunes', _('Lunes')),
        ('Martes', _('Martes')),('Miercoles', _('Miercoles')),
        ('Jueves', _('Jueves')),('Viernes', _('Viernes')),
        ('Sabado', _('Sabado')),)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    dia = models.CharField (choices = DIASEMANA, max_length = 30, null = True)
    horaDeInicio = models.TimeField(default='18:45')
    horaDeFin = models.TimeField(default='20:45')
    def __str__(self):
        return "%s - %s a %s" % (self.dia,self.horaDeInicio.strftime("%H:%M"),
            self.horaDeFin.strftime("%H:%M"))

class Comision(models.Model):
    #('id','name','profesor','materia','horario')
    NCOMISION = [('Com. 1',_("C. 1")),('Com. 2',_("C. 2")),
        ('Com. 3',_("C. 3")),('Com. 4',_("C. 4")),('Com. 5',_("C. 5")),]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField (choices=NCOMISION, max_length = 30, null = True)
    profesor =models.ManyToManyField(Profesor)
    materia = models.ManyToManyField(Materia)
    horario= models.ManyToManyField(HorarioComision)
    def Get_Materia_Name(self):
        return '\n'.join([p.label() for p in self.materia.all()])
    def Get_Profesor_Name(self):
        return '\n'.join([p.label() for p in self.profesor.all()])
    def __str__(self):
        return self.name+'  --'+str(self.Get_Materia_Name())+'--['+str(self.Get_Profesor_Name())+']'

class Alumnxs(models.Model):
    numeroLegajo = models.PositiveIntegerField(verbose_name="Numero de Legajo")
    name = models.CharField(verbose_name="Nombre",max_length=120)
    apellido = models.CharField(verbose_name="Apellido",max_length=60)
    eemail = models.EmailField(verbose_name="e-mail")
    celular = models.CharField(verbose_name="Celular",
        help_text="Sin el 15. ejemplo: 11-2020-2020",max_length=60)
    fechaDeInicio=models.DateField(verbose_name="Fecha de inicio",help_text="Fecha de inicio")
    activa = models.BooleanField(verbose_name="Alumno regular",default=True)
    carrera =models.ManyToManyField(Carrera)
    def Get_Carrera_Name(self):
        return '\n'.join([p.name for p in self.carrera.all()])
    def __str__(self):
        return "%s %s - %s" % (self.name, self.apellido,str(self.Get_Carrera_Name()))

class InscripcionMateria(models.Model):
    #('id','alumno','materia','carrera','fechaDeAlta','fechaDeBaja','activa')
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    alumno = models.ManyToManyField(Alumnxs)
    comision =  models.ManyToManyField(Comision)
    carrera = models.ManyToManyField(Carrera)
    fechaDeAlta=models.DateField(default=datetime.date.today)
    fechaDeBaja=models.DateField(null=True, blank=True)
    activa = models.BooleanField(default=True)

    def __str__(self):
        return "%s %s %s" % (self.alumno,self.comision,self.fechaDeAlta.strftime("%H:%M"))
