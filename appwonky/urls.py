from django.urls import include, path
from rest_framework import routers
#from django.conf.urls import url
from . import views

router = routers.DefaultRouter()
router.register(r'depto', views.DepartamentoViewSet)
router.register(r'carrera', views.CarreraViewSet)
router.register(r'materia', views.MateriaViewSet)
router.register(r'alumnx', views.AlumnxsViewSet)
router.register(r'profesor', views.ProfesorViewSet)
router.register(r'horario', views.HorarioViewSet)
router.register(r'inscripcion', views.InscripcionMateriaViewSet)
router.register(r'comision', views.ComisionViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
