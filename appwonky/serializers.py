from rest_framework import serializers
from rest_framework_json_api.relations import ResourceRelatedField
from .models import *
from django.utils.translation import gettext as _


class DepartamentoSerializer(serializers.HyperlinkedModelSerializer):
    """
    clase para serializar el modelo departamento
    @version 1.0.0
    """
    class Meta:
        model = Departamento
        fields = ('name', 'codigoDepartamento','decano')

class CarreraSerializer(serializers.HyperlinkedModelSerializer):
    """
    clase para serializar el modelo carrera__
    @version 1.0.0__
    """
    class Meta:
        model = Carrera
        fields = ('name', 'codigoCarrera','cuatrimestres','departamentoCarrera')
class MateriaSerializer(serializers.HyperlinkedModelSerializer):
    """
    clase para serializar el modelo Materia
    @version 1.0.0
    """
    class Meta:
        model = Materia
        fields = ('url','codigoMateria','name','cargaHoraria','activa','carrera')
class AlumnxsSerializer(serializers.HyperlinkedModelSerializer):
    """
    clase para serializar el modelo Alumnxs
    @version 1.0.0
    """

    class Meta:
        model = Alumnxs
        fields = '__all__'#( 'numeroLegajo','name','apellido','eemail','carrera')

class ProfesorSerializer(serializers.HyperlinkedModelSerializer):
    """
    clase para serializar el modelo Profesor
    @version 1.0.0
    """
    class Meta:
        model = Profesor
        fields = ('numeroLegajo','name','apellido','eemail','activa')

class HorarioSerializer(serializers.HyperlinkedModelSerializer):
    """
    clase para serializar el modelo regisMat
    @version 1.0.0
    """
    class Meta:
        model = HorarioComision
        fields = ('id','dia','horaDeInicio','horaDeFin')


class ComisionSerializer(serializers.HyperlinkedModelSerializer):
    """
    clase para serializar el modelo Comision
    @version 1.0.0
    """
    class Meta:
        model = Comision
        fields = ('id','name','profesor','materia','horario')
        #('url','profesor','materia','horario')


class InscripcionMateriaSerializer(serializers.HyperlinkedModelSerializer):
    """
    clase para serializar el modelo regisMat
    @version 1.0.0
    """

    class Meta:
        model = InscripcionMateria
        fields = ('url','id','alumno','comision','carrera','fechaDeAlta','fechaDeBaja','activa')
        #'__all__'
