from django.shortcuts import render
from rest_framework import viewsets 
from .models import *
from .serializers import *


class DepartamentoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    @Vistas de las Departamento @date 05-07-2020 @version 1.0.0
    """
    model = Departamento
    queryset = Departamento.objects.all()
    serializer_class = DepartamentoSerializer

class CarreraViewSet(viewsets.ReadOnlyModelViewSet):
    """
    @Vistas de las Carreras
    @date 05-07-2020
    @version 1.0.0
    """
    model = Carrera
    queryset = Carrera.objects.all()
    serializer_class = CarreraSerializer

class MateriaViewSet(viewsets.ReadOnlyModelViewSet):
    """
    @Vistas de las Materias
    @date 05-07-2020
    @version 1.0.0
    """
    model = Materia
    queryset = Materia.objects.all()
    serializer_class = MateriaSerializer

class AlumnxsViewSet(viewsets.ModelViewSet):
    """
    @Vistas de lxs Alumnxs
    @date 05-07-2020
    @version 1.0.0
    """
    model = Alumnxs
    queryset = Alumnxs.objects.all()
    serializer_class = AlumnxsSerializer

class ProfesorViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vistas de lxs Profesorxs
    @date 05-07-2020
    @version 1.0.0
    """
    model = Profesor
    queryset = Profesor.objects.all()
    serializer_class = ProfesorSerializer

class InscripcionMateriaViewSet(viewsets.ModelViewSet):
    """
    Vistas del registro de Inscripcion con operaciones 
    @date 05-07-2020
    @version 1.0.0
    """
    model = InscripcionMateria
    queryset = InscripcionMateria.objects.all()
    serializer_class = InscripcionMateriaSerializer

class ComisionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vistas de Comisiones
    @date 05-07-2020
    @version 1.0.0
    """
    model = Comision
    queryset = Comision.objects.all()
    serializer_class = ComisionSerializer

class HorarioViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vistas de Comisiones
    @date 05-07-2020
    @version 1.0.0
    """
    model = HorarioComision
    queryset = HorarioComision.objects.all()
    serializer_class = HorarioSerializer