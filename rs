conda create -n test-django python=3.6
conda activate test-django

conda install -c anaconda django
conda install -c anaconda pymysql
conda install -c conda-forge django-cors-headers
conda install -c conda-forge djangorestframework


django-admin startproject msite
python3 manage.py startapp appwonky
python3 manage.py createsuperuser


python3 manage.py graph_models -a -g -o imagefile_name.png
python3 manage.py graph_models appwonky -o imagefile_name2.png




* Python 3.6
* Django 3.0.3
* Django Rest Framework 3.11.0

